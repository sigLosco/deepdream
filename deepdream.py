import os
import math
import numpy as np
from functools import partial
import PIL.Image
import tensorflow as tf
import timeit
import time
from scipy.ndimage.filters import gaussian_filter
from scipy.misc import imfilter

#from tensorflow.python import debug as tf_debug

# running tensorboard with : tensorboard --logdir=/tmp/tensorflow/deepdream/logs

#To take a glimpse into the kinds of patterns that the network learned to recognize, 
#we will try to generate images that maximize the sum of activations of particular channel 
#of a particular convolutional layer of the neural network. The network we explore contains many convolutional layers,
#each of which outputs tens to hundreds of feature channels, so we have plenty of patterns to explore.

tf.app.flags.DEFINE_string("model", "tensorflow_inception_graph.pb", "Model")
tf.app.flags.DEFINE_string("input", "", "Input Image (JPG)");
tf.app.flags.DEFINE_string("output", "output", "Output prefix");
tf.app.flags.DEFINE_string("layer", "import_graph_with_input_mapping/mixed4c", "Layer name");
tf.app.flags.DEFINE_integer("feature", "-1", "Individual feature"); # default was -1
tf.app.flags.DEFINE_integer("cycles", "5", "How many cycles to run");
tf.app.flags.DEFINE_integer("octaves", "5", "How many mage octaves (scales)");
tf.app.flags.DEFINE_integer("iterations", "10", "How many gradient iterations per octave");
tf.app.flags.DEFINE_float("octave_scale", "1.4", "Octave scaling factor");
tf.app.flags.DEFINE_integer("tilesize", "512", "Size of tiles. Decrease if out of GPU memory. Increase if bad utilization.");
tf.app.flags.DEFINE_string("logdir", "/tmp/tensorflow/deepdream/logs", "Log directory")
tf.app.flags.DEFINE_string("tensboard_imgs", "0", "Number of octaves where save intermediate images on the tensorboard")

FLAGS = tf.app.flags.FLAGS

graph = tf.Graph()
sess = tf.InteractiveSession(graph=graph, config=tf.ConfigProto(log_device_placement=False)) # setting graph as default
graph_def = tf.GraphDef.FromString(open(FLAGS.model).read())
t_input = tf.placeholder(np.float32, name='image_in_input') # define the input tensor
tf.summary.image("t_input:", tf.expand_dims(t_input, 0))
#tf.summary.image("t_input:", t_input)
imagenet_mean = 117.0 # value for fix the color of pixel. (t_input expect rgba)
t_preprocessed = tf.expand_dims(t_input-imagenet_mean, 0)
#t_preprocessed = t_input-imagenet_mean
tf.summary.image("t_preprocessed", t_preprocessed)


tf.import_graph_def(graph_def, {'input':t_preprocessed}, name='import_graph_with_input_mapping') 

print "--- Available Layers: ---"
layers = []
for name in (op.name for op in graph.get_operations()):
  layer_shape = graph.get_tensor_by_name(name+':0').get_shape()
  if not layer_shape.ndims: continue
  layers.append((name, int(layer_shape[-1])))
  #print name, "Features/Channels: ", int(layer_shape[-1])
print 'Number of layers', len(layers)
print 'Total nuExpandDims/dimmber of feature channels:', sum((layer[1] for layer in layers))
print 'Chosen layer: ', graph.get_operation_by_name(FLAGS.layer);
print 'Chosen tilesize: ', FLAGS.tilesize

def T(layer):
    '''Helper for getting layer output tensor'''
    l = graph.get_tensor_by_name("%s:0"%layer)
    return l


def tffunc(*argtypes):
    '''Helper that transforms TF-graph generating function into a regular one.
    See "resize" function below.'''
    placeholders = map(tf.placeholder, argtypes) 
    def wrap(f):
        out = f(*placeholders) # out is a float image resized
        #print 'out before evaluation: ' , out # printed one time ad the beginning. Tensor without shape just float32
        
        # call wrapper for evaluate out that is like running again resizing. This way resizing is run just one time  
        def wrapper(*args, **kw): # args is a list, kw a dictionary
            #print '! wrapper !'
            #print 'placeholders predefined:', placeholders # empty placeholders of float and int
            #print 'args of the wrapper real image are the first three dimensions: ', args # one is the image array and the second is the array of the wanted size for the image
            #print 'zipping:', zip(placeholders, args) # The returned list is truncated in length to the length of the shortest argument sequence. When there are multiple arguments which are all of the same length, zip() is similar to map() with an initial argument of None.
            #print 'dict:', dict(zip(placeholders, args))
            # try sess.run instead of eval
            o = out.eval(dict(zip(placeholders, args)), session=kw.get('session')) # binding of images resized. It's passed as argument a dictionary with image (big array of float) and size (int array of 2 elements)
            #print 'out after evaluation: ', o # The image with the new size!
            #print 'out shape: ', o.shape
            return o # this is the image resized

        return wrapper
    return wrap

# Helper function that uses TF to resize an image
def resizing(img, size):
    img = tf.expand_dims(img, 0)
    #print '! resizing !'
    return tf.image.resize_bilinear(img, size)[0,:,:,:] # Resize image to size using bilinear interpolation.

''' this way calling resize is like calling wrapper ---> resize become a function
    we get two placeholders oh float and int, that after will be passed as input to the function:
    float --> image (Tensor of 4D [batch, height, width, channels]), int --> size (must be 1-D int32 Tensor of 2 elements: new_height, new_width).'''
resize = tffunc(np.float32, np.int32)(resizing) 

def calc_grad_tiled(img, t_grad, tile_size):
    '''Compute the value of tensor t_grad over the image in a tiled way.
    Random shifts are applied to the image to blur (offuscare) tile boundaries over
    multiple iterations.'''

    with tf.name_scope('calc_grad_tiled_scope'):

        merged = tf.summary.merge_all()
        #PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
        #time.sleep(2)

        sz = tile_size
        h, w = img.shape[:2] # Height and Width
        sx, sy = np.random.randint(sz, size=2) # random integers smaller than sz
        #print 'sx,sy', (sx, sy)

        #random shift of the octave (sx is a number of array (tiles) to shift on the x axis). 
        img_shift = np.roll(np.roll(img, sx, 1), sy, 0) 
     
        sigma = 1
        #img_blur = gaussian_filter(img_shift, sigma=(sigma, sigma, 0.0))

        #PIL.Image.fromarray(np.uint8(np.clip(img_blur, 0, 255))).show()
        #time.sleep(2)
        grad = np.zeros_like(img) # an octave of zeros

        y_max = max(h-(sz//2), sz)
        x_max = max(w-(sz//2), sz)

        for y in xrange(0, y_max , sz): # // is a integer division (2,3 --> 2)
            for x in xrange(0, x_max ,sz):

            	sub = img_shift[y:y+sz, x:x+sz]


                #sub = img_shift[y:y+sz,x:x+sz] # Piece of image. In case the tile size is small, it's passed a really tiny part of the image
                #print sub.shape[:3]
                PIL.Image.fromarray(np.uint8(np.clip(sub, 0, 255))).show()
                time.sleep(2)

                # np.newaxis,: # for adding one dimension
                g, summary = sess.run([t_grad, merged], {t_input:sub})
                #PIL.Image.fromarray(np.uint8(np.clip(g, 0, 255))).show()
                #time.sleep(3)
                #print 'g',g # super small values
                grad[y:y+sz,x:x+sz] = g # the tiny image after gradient is inserted inside the bigger original octave size
                #PIL.Image.fromarray(np.uint8(np.clip(grad, 0, 255))).show()
                #time.sleep(3)

        #return grad, summary 
        return np.roll(np.roll(grad, -sx, 1), -sy, 0), summary # re-shifting octave as the initial input

def render_deepdream(t_obj, img, graph_writer, i_frame, board_imgs,
                     iter_n=10, step_grad=1, octave_n=5, octave_scale=1.4):

    with tf.name_scope('render_deepdream_scope'):

        t_score = tf.reduce_mean(t_obj, name='Reduce_mean_of_layer') # defining the optimization objective
        tf.summary.scalar('t_score', t_score)
        
        #t_score: A Tensor or list of tensors to be differentiated.
        #t_input: A Tensor or list of tensors to be used for differentiation.
        #t_grad returns a list of sum(dy/dx) for each x in t_input.
        t_grad = tf.gradients(t_score, t_input)[0]
        tf.summary.histogram('t_grad', t_grad)
        tf.summary.scalar('t_grad_mean', tf.reduce_mean(t_grad))

        octaves = []
        for i in xrange(octave_n-1): # 0,1,2,3
            hw = img.shape[:2] # gets height and width of the original dimensions

            lo = resize(img, np.int32(np.float32(hw)/octave_scale)) # resizing image accordingly the scale (SCALE DOWN). 

            #print 'lo, original image has now this shape:', lo.shape
            #PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
            PIL.Image.fromarray(np.uint8(np.clip(resize(lo,hw), 0, 255))).show()
            time.sleep(2) # delays for 5 seconds

            hi = img-resize(lo, hw) # in order to underline contrasts in the image: substract to the originale image one RESCALED UP (from a lower resolution)
            
            #PIL.Image.fromarray(np.uint8(np.clip(hi, 0, 255))).show()
            #print 'resize(lo,hw)', resize(lo,hw).shape
            #print 'hi:', hi.shape
            #time.sleep(10)

            img = lo # the next image is the resized one
            octaves.append(hi) # the first element of octaves has to original image shape (octaves[0]) 

        for octave in xrange(octave_n): # 0,1,2,3,4
            start_octave = timeit.default_timer()
            if octave>0: # jump first octave (the first one inserted, the original image)
                hi = octaves[-octave] # a = [1,2,3] a[-1] last element (returns 3). It gets last octave inserted
                
                #print 'hi shape', hi.shape
                PIL.Image.fromarray(np.uint8(np.clip(hi, 0, 255))).show() # Dark octave
                time.sleep(2)
                #PIL.Image.fromarray(np.uint8(np.clip(resize(img, hi.shape[:2]), 0, 255))).show() # Scaling UP
                
                img = resize(img, hi.shape[:2])+hi # Scaling UP to hi.shape and then add the Dark image --> you get Good resolution

                PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
                time.sleep(2) # delays for 5 seconds

            for i in xrange(iter_n): # first img passed is the original one (smallest size)
                with tf.name_scope('tiled_grad_computation'):
                    #start_iteration = timeit.default_timer()

                	#PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
                    #time.sleep(2)

                    step = i_frame * (octave_n*iter_n) + (octave*iter_n) + i


                    g, summary = calc_grad_tiled(img, t_grad, FLAGS.tilesize) # g is the output of the gradient on that octave
                    
                    graph_writer.add_summary(summary, step)

                    #PIL.Image.fromarray(np.uint8(np.clip(g, 0, 255))).show()
                    #time.sleep(3) 
                    
			        # Blur the gradient with different amounts and add
			        # them together. The blur amount is also increased
			        # during the optimization. This was found to give
			        # nice, smooth images. You can try and change the formulas.
			        # The blur-amount is called sigma (0=no blur, 1=low blur, etc.)
			        # We could call gaussian_filter(grad, sigma=(sigma, sigma, 0.0))
			        # which would not blur the colour-channel. This tends to
			        # give psychadelic / pastel colours in the resulting images.
			        # When the colour-channel is also blurred the colours of the
			        # input image are mostly retained in the output image. 
                    #grad = g
                    sigma = (i * 4.0) / iter_n + 0.5
                    grad_smooth1 = gaussian_filter(g, sigma=(sigma, sigma, sigma))
                    grad_smooth2 = gaussian_filter(g, sigma=(sigma*2, sigma*2, sigma*2))
                    grad_smooth3 = gaussian_filter(g, sigma=(sigma*0.5, sigma*0.5 , sigma*0.5))
                    grad = (grad_smooth1 + grad_smooth2 + grad_smooth3) 


                    #print '(np.abs(g).mean()): ', (np.abs(g).mean())
                    #print '(step / (np.abs(g).mean()+1e-7)): ', (step / (np.abs(g).mean()+1e-7)) # high number
                    #print 'g', g
                    #print 'g after mult', g*(step / (np.abs(g).mean()+1e-7))
                    #time.sleep(20)
                    
                    img += grad*(step_grad / (np.abs(grad).mean()+1e-7)) # abs ---> return the absolute value of a number. 1*10 alla -7
                    


                    # it just works with cpu
                    if i_frame == 0 and octave < board_imgs:
                       image_shaped_input = tf.expand_dims(img,0)
                       tf.summary.image("octaves: %s"%octave, image_shaped_input )

                    #PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
                    #time.sleep(2)
                    #print 'img', img

                    #stop_iteration = timeit.default_timer()
                    #print 'Iteration', i ,' Performed in: %s seconds' % round((stop_iteration - start_iteration),2)
            
            stop_octave = timeit.default_timer()
            print 'Octave', octave , " Res: ", img.shape, ' Performed in: %s seconds' % round((stop_octave - start_octave),2)
            #PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
            #time.sleep(2)  
        
        return img

def main(_):
  
  if tf.gfile.Exists(FLAGS.logdir):
    tf.gfile.DeleteRecursively(FLAGS.logdir)
  tf.gfile.MakeDirs(FLAGS.logdir)

  start = timeit.default_timer()
  
  if FLAGS.input == 'random':
    img = np.random.uniform(size=(299,299,3)) + 100
    PIL.Image.fromarray(np.uint8(np.clip(img, 0, 255))).show()
  else:
    img = np.float32(PIL.Image.open(FLAGS.input));

  # blur, contour, detail, edge_enhance, edge_enhance_more, emboss, find_edges, smooth, smooth_more, sharpen.
  # sharpened = imfilter(img, 'edge_enhance')
  # PIL.Image.fromarray(np.uint8(np.clip(sharpened, 0, 255))).show()

  # Make RGB if greyscale:
  if len(img.shape)==2 or img.shape[2] == 1:
    img = np.stack([img]*3, axis=2)
  
  graph_writer = tf.summary.FileWriter(FLAGS.logdir) # we must write the graph now, at the end

  for i_frame in range(FLAGS.cycles): # range has static list, xrange creates an object list better for high number of iteration
    start_cycle = timeit.default_timer()
    print "Cycle",i_frame,"starts  Res:", img.shape
    t_obj = tf.square(T(FLAGS.layer), name='Squaring_layer') # fetching layer with last feature (512) and then squaring all the values of the layer

    if FLAGS.feature >= 0:
      t_obj = T(FLAGS.layer)[:,:,:,FLAGS.feature] #fetch the same layer but with this feature. picking some feature channel to visualize

    img = render_deepdream(t_obj, img, graph_writer, i_frame, 
        board_imgs = np.uint8(FLAGS.tensboard_imgs),
        iter_n = FLAGS.iterations,
        octave_n = FLAGS.octaves,
        octave_scale = FLAGS.octave_scale)
    stop_cycle = timeit.default_timer()
    print 'Cycle', i_frame , " Res: ", img.shape, ' Performed in: %s seconds' % round((stop_cycle - start_cycle), 2)
    print "Saving image", i_frame
    img = np.uint8(np.clip(img, 0, 255)) # getting int values again
    PIL.Image.fromarray(img).save("%s_%05d.jpg"%(FLAGS.output, i_frame), "jpeg")
    
    #img = np.float32(img)

  graph_writer.add_graph(sess.graph)
  graph_writer.close()
  stop = timeit.default_timer()
  print 'deep dream running time = %s seconds' % round((stop - start),2)

if __name__ == "__main__":
  tf.app.run()
